<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-22 07:12:11
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-02 12:57:02
*/


use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
    
    <div class="table-laporan">
    
    <p>
        <?= Html::a('Export Excel', ['export-excel?dari='.$dari.'&sampai='.$sampai], ['class'=>'btn btn-info']); ?>
        <?= Html::a('Export Pdf', ['export-pdf?dari='.$dari.'&sampai='.$sampai], ['class'=>'btn btn-warning']); ?>
    </p>
    
    <div class="table-responsive">
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'Pertama',
            'lastPageLabel'  => 'Terakhir'
        ],
        'rowOptions' => function($model) {
            if ($model->status==='Menunggu') return ['class' => 'danger'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tgl_masuk',
                'headerOptions' => ['style' => 'width:14%'],
                'value' => function($model){
                    return $model->tgl_masuk;
                    // return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => ' Contoh: 2018-12-23'
                 ]
            ],
            [
                'attribute' => 'asal_rujukan_text',
                'header' => '<font color="#2fa4e">Asal Rujukan</font>',
                'value' => function ($model) {
                    return $model->asal_rujukan_text;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'tujuan_rujukan_text',
                'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
                'value' => function ($model) {
                    return $model->tujuan_rujukan_text;
                },
                'format' => 'html',
            ],
            'nama',
            'tglahir',
            [
                'attribute' => 'jk',
                'value' => function ($model) {
                    if($model->jk)
                        return $model->jk;
                    else
                        return '';
                },
                'format' => 'html',
            ],
            'diagnosa:ntext',
            'status',
            'info_balik:ntext',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
    </div>