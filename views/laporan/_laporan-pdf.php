<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-06-04 10:38:30
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 11:24:24
*/

?>

<table style="width:100%; margin-bottom: 35px;">
	<tbody>
		<tr>
			<td style="text-align:center;" width="100%">
				<h4 style="padding-top:8px;margin-bottom:15px;margin-left:15%">
				<strong>
					LAPORAN SISTEM INFORMASI RUJUKAN PONEK TERINTEGRASI
				</strong>
				</h4>
				<h4 style="padding-top:8px;margin-bottom:15px;margin-left:15%">
				<strong>
					<?= strtoupper($pengguna) ?>
				</strong>
				</h4>
				<h4 style="padding-top:8px;margin-bottom:15px;margin-left:15%">
				<strong>
					<?= strtoupper($judulDokumen) ?>
				</strong>
				</h4>
			</td>
		</tr>
	</tbody>
</table>


<table class="table table-bordered" style="padding-top:50px">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Masuk</th>
			<th>Asal Rujukan</th>
			<th>Tujuan Rujukan</th>
			<th>Nama</th>
			<th>Tanggal Lahir</th>
			<th>Jenis Kelamin</th>
			<th>Diagnosa</th>
			<th>Status</th>
			<th>Info Balik</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no = 1;
			foreach($model as $m)
			{
				echo '<tr>';

					echo '<td>'.$no.'</td>';
					echo '<td>'.Yii::$app->formatter->asDate($m->tgl_masuk).'</td>';
					echo '<td>'.$m->getRelationAsalRujukan().'</td>';
					echo '<td>'.$m->getRelationTujuanRujukan().'</td>';
					echo '<td>'.$m->nama.'</td>';
					echo '<td>'.Yii::$app->formatter->asDate($m->tglahir).'</td>';
					echo '<td>'.$m->jk.'</td>';
					echo '<td>'.$m->diagnosa.'</td>';
					echo '<td>'.$m->status.'</td>';
					echo '<td>'.$m->info_balik.'</td>';

				echo '</tr>';
				$no++;
			}
		?>
	</tbody>
</table>