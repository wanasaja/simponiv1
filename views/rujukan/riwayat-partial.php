<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-27 05:01:17
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-27 05:01:59
*/


use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\Penolong;
use yii\widgets\Pjax;

?>

<div class="table-responsive tabel-riwayat-rujukan">
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'pager' => [
        'firstPageLabel' => 'Pertama',
        'lastPageLabel'  => 'Terakhir'
    ],
    'rowOptions' => function($model) {
        if ($model->status==='Menunggu') return ['class' => 'warning'];
    },
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        // 'id',
        [
            'attribute' => 'tgl_masuk',
            'headerOptions' => ['style' => 'width:14%'],
            'value' => function($model){
                return $model->tgl_masuk;
                // return Yii::$app->formatter->asDate($model->tgl_masuk);
            },
            'filterInputOptions' => [
                'class'       => 'form-control',
                'placeholder' => 'Contoh: 2018-12-23'
             ]
        ],
        'nama',
        [
            'attribute' => 'asal_rujukan_text',
            'header' => '<font color="#2fa4e">Asal Rujukan</font>',
            'value' => function ($model) {
                return $model->asal_rujukan_text;
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'tujuan_rujukan_text',
            'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
            'value' => function ($model) {
                return $model->tujuan_rujukan_text;
            },
            'format' => 'html',
        ],
        'alasan_rujukan:ntext',
        'diagnosa:ntext',
        'info_balik:ntext',
        [
            'headerOptions' => ['class' => 'text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'attribute' => 'status',
            'value' => function ($model) {
                return Penolong::label($model->status);
            },
            'format' => 'html',
        ],

        [
            'class'    => 'yii\grid\ActionColumn',
            'header' => 'Aksi',
            'headerOptions' => ['style' => 'color:#2fa4e7; text-align:center;'],
            'contentOptions' => ['class' => 'text-center'],
            'template' => '{lihat}',
            'visibleButtons'=>[
                'lihat'=> function($model){
                      return $model->status==='Diterima' || $model->status==='Tidak Diterima';
                 },
            ],
            'buttons'  => [                    
                'lihat' => function ($url, $model) {
                    $url = Url::to(['rujukan/cetak', 'id' => $model->id]);
                    return Html::a('<span class="">Print</span>', $url, [
                        'class' => 'btn btn-info btn-xs',
                        'style' => 'width:100%',
                        // 'target' => '_blank',
                        // 'data-toggle'=>'tooltip', 
                        'title'=>'Akan Mencetak PDF di Tab Baru.',
                        'data' => [
                            'method' => 'post',
                        ],
                    ]);
                },
            ]
        ],        
    ],
]); ?>
<?php Pjax::end(); ?>
</div>