<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-12 12:24:22
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-18 11:01:34
*/

use yii\helpers\Url;
use yii\widgets\DetailView;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

$pesanQr = $model->tgl_masuk.'\n'.$model->nama.'\n'.$model->status.'\n'.$model->info_balik;

$pesanQr = 'aku manja';

?>

<p>
	
	<table style="width:100%">
		<tbody>
			<tr>
				<td style="" width="20%">
					<barcode code="<?= $pesanQr ?>" disableborder="1" type="QR" class="barcode" size="0.8" error="M"/>
				</td>
				<td style="" width="80%"><h3 style="padding-top:8px;margin-bottom:15px;margin-left:15%"><strong>Sistem Informasi Rujukan PONEK</strong></h3></td>
			</tr>
		</tbody>
	</table>
<!-- <h3 style="padding-top:15px;margin-bottom:28px;text-align: center;"><strong>Sistem Informasi Rujukan PONEK</strong></h3> -->
</p>
	

<!-- <div class="panel panel-default">
  <div class="panel-body">

	<h3 style="margin-top 5px;margin-bottom:28px;text-align: center;"><u>Data Umum Pasien</u></h3>

    <?= DetailView::widget([
        'model' => $model,
        'template'=>'<tr><th width="40%">{label}</th><td>{value}</td></tr>',
        'options' => ['class' => 'table table-bordered detail-view'],
        'attributes' => [
        	[
                'attribute' => 'tgl_masuk',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
            ],
            'jenis',
            'nama',
            'jk',
            'tplahir',
            [
                'attribute' => 'tglahir',
                'value' => function($model){
                	if($model->tglahir)
	                    return Yii::$app->formatter->asDate($model->tglahir);
	                else
	                	return '';
                },
            ],
            'umur',
            'alamat:ntext',
            'cara_bayar',
            [
                'attribute' => 'nik',
                'value' => function($model){
                	if($model->nik)
	                    return Yii::$app->formatter->asDate($model->nik);
	                else
	                	return '';
                },
            ],
            [
                'attribute' => 'no_bpjs_jkd',
                'value' => function($model){
                	if($model->no_bpjs_jkd)
	                    return Yii::$app->formatter->asDate($model->no_bpjs_jkd);
	                else
	                	return '';
                },
            ],
        ],
    ]) ?>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">

	<h3 style="margin-top 5px;margin-bottom:28px;text-align: center;"><u>Rujukan & Resume Pasien</u></h3>

    <?= DetailView::widget([
        'model' => $model,
        'template'=>'<tr><th width="40%">{label}</th><td>{value}</td></tr>',
        'options' => ['class' => 'table table-bordered detail-view'],
        'attributes' => [
            'asal_rujukan_text',
            'tujuan_rujukan_text',
            'alasan_rujukan:ntext',
            'anamnesa:ntext',
            'kesadaran',
            [
                'attribute' => 'tekanan_darah',
                'value' => function($model){
                    return '<div class="row"><span class="col-md-1" style="margin-right:25%">'.$model->tekanan_darah.'</span> mmHg</div>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'nadi',
                'value' => function($model){
                    return '<div class="row"><span class="col-md-1" style="margin-right:25%">'.$model->nadi.'</span> x/menit</div>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'suhu',
                'value' => function($model){
                    return '<div class="row"><span class="col-md-1" style="margin-right:25%">'.$model->suhu.'</span> C</div>';
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'pernapasan',
                'value' => function($model){
                    return '<div class="row"><span class="col-md-1" style="margin-right:25%">'.$model->pernapasan.'</span> x/menit</div>';
                },
                'format' => 'html'
            ],
            'nyeri',
            'pemeriksaan_fisik:ntext',
            'pemeriksaan_penunjang:ntext',
            'diagnosa:ntext',
            'tindakan_yg_sdh_diberikan:ntext',
            
            'status',
            'info_balik:ntext',
        ],
    ]) ?>
  </div>
</div> -->




<div class="panel panel-default" style="">
	<div class="panel-body">

	<h4 style="margin-bottom:15px;text-align: center;"><u>Data Umum Pasien</u></h4>

	<table id="w0" class="table detail-view">
		<tbody>
			<tr>
				<th width="40%" style="padding:3px;">Tanggal Masuk</th><td style="padding:3px;"><?= Yii::$app->formatter->asDate($model->tgl_masuk) ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Jenis Pasien</th><td style="padding:3px;"><?= $model->jenis ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Nama</th><td style="padding:3px;"><?= $model->nama ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Jenis Kelamin</th><td style="padding:3px;"><?= $model->jk ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Tempat Lahir</th><td style="padding:3px;"><?= $model->tplahir ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Tanggal Lahir</th><td style="padding:3px;"><?= $model->tglahir ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Umur</th><td style="padding:3px;"><?= $model->umur ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Alamat</th><td style="padding:3px;"><?= $model->alamat ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Cara Bayar</th><td style="padding:3px;"><?= $model->cara_bayar ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">NIK</th><td style="padding:3px;"><?= $model->nik ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">No BPJS/JKD</th><td style="padding:3px;"><?= $model->no_bpjs_jkd ?></td>
			</tr>
		</tbody>
	</table>  
	</div>
</div>



<div class="panel panel-default">
	<div class="panel-body">

		<h4 style="margin-bottom:15px;text-align: center;"><u>Rujukan &amp; Resume Pasien</u></h4>

		<table id="w1" class="table detail-view">
			<tbody>
				<tr>
					<th width="40%" style="padding:3px;">Asal Rujukan Text</th><td style="padding:3px;"><?= $model->asal_rujukan_text ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tujuan Rujukan Text</th><td style="padding:3px;"><?= $model->tujuan_rujukan_text ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Alasan Rujukan</th><td style="padding:3px;"><?= $model->alasan_rujukan ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Anamnesa</th><td style="padding:3px;"><?= $model->anamnesa ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Kesadaran</th><td style="padding:3px;"><?= $model->kesadaran ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tekanan Darah</th><td style="padding:3px;"><div class="row"><span class="col-md-6" style="margin-right:25%;"><?= $model->tekanan_darah ?></span> <i>mmHg</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Nadi</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->nadi ?></span> <i>x/menit</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Suhu</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->suhu ?></span> <i>C</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pernapasan</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->pernapasan ?></span> <i>x/menit</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Nyeri</th><td style="padding:3px;"><?= $model->nyeri ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pemeriksaan Fisik</th><td style="padding:3px;"><?= $model->pemeriksaan_fisik ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pemeriksaan Penunjang</th><td style="padding:3px;"><?= $model->pemeriksaan_penunjang ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Diagnosa</th><td style="padding:3px;"><?= $model->diagnosa ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tindakan Yang Sudah Diberikan</th><td style="padding:3px;"><?= $model->tindakan_yg_sdh_diberikan ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Status</th><td style="padding:3px;"><?= $model->status ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Info Balik</th><td style="padding:3px;"><?= $model->info_balik ?></td>
				</tr>
			</tbody>
		</table>  
	</div>
</div>