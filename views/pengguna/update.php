<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-01 22:07:36
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-12 21:30:18
*/

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Pengguna */

$this->title = 'Update Pengguna';
$this->params['breadcrumbs'][] = ['label' => 'Pengguna', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengguna-update">
	
	<div class="row" style="padding-bottom: 1.5%">
		<div class="col-md-12">
			<?= Html::a('Kembali', Yii::$app->request->referrer, ['class'=>'btn btn-info pull-right'])?>
		</div>
	</div>
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
