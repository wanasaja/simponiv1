<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property int $id
 * @property string $waktu
 * @property int $dari
 * @property int $untuk
 * @property string $isi
 */
class Chat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['waktu', 'dari', 'untuk', 'isi'], 'required'],
            [['waktu'], 'safe'],
            [['dari', 'untuk', 'baca'], 'integer'],
            [['isi'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'waktu' => 'Waktu',
            'dari' => 'Dari',
            'untuk' => 'Untuk',
            'isi' => 'Isi',
            'baca' => 'Dibaca',
        ];
    }

    public static function kotakChat($dari, $untuk)
    {
        return SELF::find()
                // ->select('*, '.SELF::getRelationDari().'as dariText')
                ->andWhere(['and',
                    ['dari' => $dari],
                    ['untuk' => $untuk]
                ])
                ->orWhere(['and',
                    ['dari' => $untuk],
                    ['untuk' => $dari]
                ])
                ->all();
    }

    public function getRelasidari()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'dari']);
    }
    public function getDari_text() 
    {
        return $this->relasidari->nama_rs_puskesmas;
    }

    public function getRelasiuntuk()
    {
        return $this->hasOne(Pengguna::className(), ['id' => 'untuk']);
    }
    public function getUntuk_text() 
    {
        return $this->relasiuntuk->nama_rs_puskesmas;
    }

}
