-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2018 at 02:32 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petalabumi-ponek`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `waktu` datetime NOT NULL,
  `dari` int(11) NOT NULL,
  `untuk` int(11) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `waktu`, `dari`, `untuk`, `isi`) VALUES
(1, '2018-06-05 10:35:20', 10, 2, 'kamu ngapain???'),
(2, '2018-06-05 11:35:40', 2, 10, 'iya, lg makan niiii'),
(3, '2018-06-05 12:36:17', 10, 2, 'main aja'),
(4, '2018-06-05 14:23:47', 2, 10, 'pengajian'),
(5, '2018-06-05 14:31:23', 2, 10, 'hehe'),
(6, '2018-06-05 14:37:30', 2, 10, 'dimana?'),
(7, '2018-06-06 10:06:08', 2, 10, 'iya??'),
(8, '2018-06-06 10:07:40', 2, 10, 'kam'),
(9, '2018-06-06 10:07:55', 2, 10, 'ngapa?'),
(10, '2018-06-06 10:07:57', 2, 10, 'ngapa?'),
(11, '2018-06-06 10:08:05', 2, 10, 'ayam'),
(12, '2018-06-06 10:09:26', 2, 10, 'dimana?'),
(13, '2018-06-06 10:09:31', 2, 10, 'iya kah?'),
(14, '2018-06-06 10:10:29', 2, 10, 'dia'),
(15, '2018-06-06 10:11:13', 2, 10, 'ngapa?'),
(16, '2018-06-06 10:19:23', 2, 10, 'mangaaaaaaaaa'),
(17, '2018-06-06 10:21:51', 2, 10, 'sekolah maa'),
(18, '2018-06-06 10:23:41', 2, 10, 'asd'),
(19, '2018-06-06 10:24:43', 2, 11, 'dimana?'),
(20, '2018-06-06 10:26:57', 2, 10, 'gagagaga'),
(21, '2018-06-06 10:27:00', 2, 10, 'asdas'),
(22, '2018-06-06 10:27:03', 2, 10, 'sdfsdf'),
(23, '2018-06-06 10:27:13', 2, 10, 'sdfsdf'),
(24, '2018-06-06 10:27:16', 2, 10, 'sfdsf'),
(25, '2018-06-06 10:27:21', 2, 10, 'asdasd'),
(26, '2018-06-06 10:27:44', 2, 10, 'koko'),
(27, '2018-06-06 10:27:47', 2, 10, 'as'),
(28, '2018-06-06 10:28:18', 2, 10, 'ger'),
(29, '2018-06-06 10:28:24', 2, 10, 'asdas'),
(30, '2018-06-06 10:28:27', 2, 10, 'asd'),
(31, '2018-06-06 10:29:31', 2, 10, 'bujang'),
(32, '2018-06-06 10:29:37', 2, 10, 'maeen'),
(33, '2018-06-06 10:29:47', 2, 10, 'iya yaaa'),
(34, '2018-06-06 10:29:50', 2, 10, 'asdads'),
(35, '2018-06-06 10:29:53', 2, 10, 'assdasd'),
(36, '2018-06-06 10:30:17', 2, 10, 'iya'),
(37, '2018-06-06 10:30:18', 2, 10, 'me'),
(38, '2018-06-06 10:30:20', 2, 10, 'asdf'),
(39, '2018-06-06 10:32:02', 2, 10, 'koe'),
(40, '2018-06-06 10:32:29', 2, 10, 'as'),
(41, '2018-06-06 10:36:12', 2, 10, 'asdas'),
(42, '2018-06-06 10:36:16', 2, 10, 'asdad'),
(43, '2018-06-06 10:36:17', 2, 10, 'asd'),
(44, '2018-06-06 10:36:17', 2, 10, 'asd'),
(45, '2018-06-06 10:36:20', 2, 10, 'asd'),
(46, '2018-06-06 10:36:23', 2, 10, 'ade'),
(47, '2018-06-06 10:36:25', 2, 10, 'ade'),
(48, '2018-06-06 10:38:11', 2, 10, 'gegge'),
(49, '2018-06-06 10:38:26', 2, 10, 'kemana yaang'),
(50, '2018-06-06 10:39:00', 2, 10, 'boleh'),
(51, '2018-06-06 10:39:42', 2, 10, 'gimana'),
(52, '2018-06-06 10:42:33', 2, 10, 'asdf'),
(53, '2018-06-06 10:42:55', 2, 10, 'koko'),
(54, '2018-06-06 10:43:23', 2, 10, 'kalian kemana?'),
(55, '2018-06-06 10:50:39', 2, 10, 'iy amen?'),
(56, '2018-06-06 10:50:43', 2, 10, 'heheh'),
(57, '2018-06-06 10:59:21', 2, 10, 'dimana men?'),
(58, '2018-06-06 10:59:29', 10, 2, 'iya? kamu dimana?'),
(59, '2018-06-06 11:00:23', 10, 2, 'haa'),
(60, '2018-06-06 11:01:24', 2, 10, 'ngapa mbak?'),
(61, '2018-06-06 11:01:31', 10, 2, 'gak papap mas'),
(62, '2018-06-06 11:01:41', 2, 10, 'iya ya?'),
(63, '2018-06-06 11:02:04', 2, 10, 'tunggu aja'),
(64, '2018-06-06 11:02:16', 10, 2, 'ok ok'),
(65, '2018-06-06 11:05:26', 2, 10, 'gimana bro'),
(66, '2018-06-06 11:06:37', 2, 10, 'hoi'),
(67, '2018-06-06 11:07:15', 10, 2, 'tak ape'),
(68, '2018-06-06 11:07:18', 2, 10, 'iye ke'),
(69, '2018-06-06 11:07:56', 2, 10, 'iya'),
(70, '2018-06-06 11:08:01', 10, 2, 'mbo'),
(71, '2018-06-06 11:08:48', 10, 2, 'tres'),
(72, '2018-06-06 11:31:58', 2, 10, 'apooo'),
(73, '2018-06-06 11:32:55', 11, 2, 'ngapo bang'),
(74, '2018-06-06 11:33:01', 2, 11, 'tak apo doo'),
(75, '2018-06-06 12:06:05', 11, 2, 'why\\'),
(76, '2018-06-06 14:46:20', 11, 2, 'iyak'),
(77, '2018-06-06 14:46:35', 2, 11, 'geg'),
(78, '2018-06-06 14:50:50', 2, 10, 'iya kali'),
(79, '2018-06-06 14:50:56', 2, 10, 'hehehe'),
(80, '2018-06-06 14:51:01', 2, 10, 'kuko'),
(81, '2018-06-06 15:18:32', 2, 11, 'apa men'),
(82, '2018-06-06 15:18:40', 11, 2, 'gap papap');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(15) NOT NULL COMMENT '[ super_admin, admin, user ]',
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `nama_rs_puskesmas` varchar(250) NOT NULL,
  `kontak` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `hak_akses`, `username`, `password`, `nama_rs_puskesmas`, `kontak`, `email`, `alamat`) VALUES
(1, 'super_admin', 'admin', 'admin', 'SUPER_ADMIN', '(0761) 23024', 'petalabumi@gmail.com', 'jl. dkakek sudrman'),
(2, 'admin', 'petala', 'petala', 'RSUD Petala Bumi', '(0761) 23024', 'rsudpetalabumi@riau.go.id', 'Jl. Dr. Soetomo No. 65'),
(10, 'user', 'stiga', 'stiga', 'Puskesmas Simpang Tiga', '(0761) 674763', 'simpangtiga@gmail.com', 'Jl. Kaharuddin Nst, Maharatu, Marpoyan Damai, Kota Pekanbaru, Riau 28288'),
(11, 'user', 'smulyo', 'smulyo', 'Puskesmas Sidomulyo', '(0761) 63170', '', 'Delima, Tampan, Pekanbaru City, Riau 28293');

-- --------------------------------------------------------

--
-- Table structure for table `rujukan`
--

CREATE TABLE `rujukan` (
  `id` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jenis` varchar(4) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jk` varchar(25) DEFAULT NULL,
  `tplahir` varchar(100) NOT NULL,
  `tglahir` date DEFAULT NULL,
  `umur` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `cara_bayar` varchar(10) NOT NULL,
  `nik` bigint(16) DEFAULT NULL,
  `no_bpjs_jkd` int(16) DEFAULT NULL,
  `asal_rujukan` int(11) NOT NULL,
  `tujuan_rujukan` int(11) NOT NULL,
  `alasan_rujukan` text NOT NULL,
  `anamnesa` text NOT NULL,
  `kesadaran` varchar(12) NOT NULL,
  `tekanan_darah` varchar(10) NOT NULL,
  `nadi` int(11) NOT NULL,
  `suhu` varchar(10) NOT NULL,
  `pernapasan` int(11) NOT NULL,
  `nyeri` varchar(25) NOT NULL,
  `pemeriksaan_fisik` text NOT NULL,
  `pemeriksaan_penunjang` text NOT NULL,
  `diagnosa` text NOT NULL,
  `tindakan_yg_sdh_diberikan` text NOT NULL,
  `info_balik` text NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rujukan`
--

INSERT INTO `rujukan` (`id`, `tgl_masuk`, `jenis`, `nama`, `jk`, `tplahir`, `tglahir`, `umur`, `alamat`, `cara_bayar`, `nik`, `no_bpjs_jkd`, `asal_rujukan`, `tujuan_rujukan`, `alasan_rujukan`, `anamnesa`, `kesadaran`, `tekanan_darah`, `nadi`, `suhu`, `pernapasan`, `nyeri`, `pemeriksaan_fisik`, `pemeriksaan_penunjang`, `diagnosa`, `tindakan_yg_sdh_diberikan`, `info_balik`, `status`) VALUES
(40, '2018-04-01', 'Ibu', 'eko purnando', '', 'paknign', '2018-05-10', '0 Tahun 0 Bulan 13 Hari ', 'alamat saya', 'BPJS', 2342343634, 2353455, 10, 2, 'biar enak', 'apaan tu?', 'Tidak Sadar', '123', 234, '0', 0, 'Nyeri Ringan', 'uhhh', 'kaligrafi', 'diag', 'makan banyak', 'iya deh', 'Diterima'),
(41, '2018-05-23', 'Ibu', 'rita marliya', '', 'medan', '2018-05-03', '0 Tahun 0 Bulan 20 Hari ', 'jl. karet', 'UMUM', NULL, NULL, 11, 2, 'berdarah', 'biasa aja', 'Sadar', '5', 7, '3', 9, 'Nyeri Sedang', 'berat kali bah', 'gak ada kok', 'cepetan ya', 'kompresssss', 'dsdf', 'Tidak Diterima'),
(42, '2018-05-18', 'Ibu', 'dika kusuma', 'Laki-laki', 'paknign', '2018-05-10', '0 Tahun 0 Bulan 13 Hari ', 'alamat saya', 'BPJS', 2342343634, 2353455, 10, 2, 'biar enak', 'apaan tu?', 'Tidak Sadar', '123', 234, '0', 0, 'Nyeri Ringan', 'uhhh', 'kaligrafi', 'diag', 'makan banyak', 'iya deh', 'Diterima'),
(43, '2018-06-16', 'Ibu', 'puspita sari', '', 'paknign', '2018-05-10', '0 Tahun 0 Bulan 13 Hari ', 'alamat saya', 'BPJS', 2342343634, 2353455, 10, 2, 'biar enak', 'apaan tu?', 'Tidak Sadar', '123', 234, '0', 0, 'Nyeri Ringan', 'uhhh', 'kaligrafi', 'diag', 'makan banyak', 'iya deh', 'Tidak Diterima'),
(44, '2018-04-11', 'Ibu', 'paijo bejo', 'Laki-laki', 'medan', '2018-05-03', '0 Tahun 0 Bulan 20 Hari ', 'jl. karet', 'UMUM', NULL, NULL, 11, 2, 'berdarah', 'biasa aja', 'Sadar', '5', 7, '3', 9, 'Nyeri Sedang', 'berat kali bah', 'gak ada kok', 'cepetan ya', 'kompresssss', 'dsdf', 'Tidak Diterima'),
(45, '2018-07-01', 'Ibu', 'yurliana wati', '', 'paknign', '2018-05-10', '0 Tahun 0 Bulan 13 Hari ', 'alamat saya', 'BPJS', 2342343634, 2353455, 10, 2, 'biar enak', 'iya deh iya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya dehiya deh', 'Tidak Sadar', '123', 234, '0', 0, 'Nyeri Ringan', 'uhhh', 'kaligrafi', 'diag', 'makan banyak', 'kami kemanaaaaaaa', 'Diterima'),
(46, '2018-04-11', 'Ibu', 'rahmat put', 'Perempuan', 'medan', '2018-05-03', '0 Tahun 0 Bulan 20 Hari ', 'jl. karet', 'UMUM', NULL, NULL, 11, 2, 'berdarah', 'biasa aja', 'Sadar', '5', 7, '3', 9, 'Nyeri Sedang', 'berat kali bah', 'gak ada kok', 'cepetan ya', 'kompresssss', 'dsdf', 'Diterima'),
(47, '2018-05-25', 'Ibu', 'Mulyani Handayano', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'heheh', 'Tidak Diterima'),
(48, '2018-05-25', 'Bayi', 'pak parjo', 'Laki-laki', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'lanjutkan baang', 'Diterima'),
(49, '2018-05-25', 'Bayi', 'bu jeka', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'makan bareng', 'Tidak Diterima'),
(50, '2018-05-25', 'Ibu', 'dian maya', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'ngapain', 'Tidak Diterima'),
(51, '2018-05-25', 'Ibu', 'ziaya', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'hohoho', 'Tidak Diterima'),
(52, '2018-05-25', 'Ibu', 'jojo', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'heheheh', 'Diterima'),
(53, '2018-05-25', 'Ibu', 'noraya', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'ewfsdf', 'Tidak Diterima'),
(54, '2018-05-25', 'Ibu', 'dia geleng', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'makan malam', 'Tidak Diterima'),
(55, '2018-05-25', 'Ibu', 'dia geleng', '', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'asdasd', 'Diterima'),
(56, '2018-05-25', 'Ibu', 'aliya', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'kmn', 'Tidak Diterima'),
(57, '2018-05-25', 'Ibu', 'annisa', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'asdasd', 'Tidak Diterima'),
(58, '2018-05-25', 'Bayi', 'kiki', 'Laki-laki', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'opopopo', 'Tidak Diterima'),
(59, '2018-05-25', 'Ibu', 'riko', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', 'lanjut eee', 'Tidak Diterima'),
(60, '2018-05-25', 'Ibu', 'rizki rahayu', 'Perempuan', 'marpoyan', '2018-05-01', '0 Tahun 0 Bulan 24 Hari ', 'jl. hang jebat', 'BPJS', 987, 657, 11, 2, 'tak kuat', 'biasa aja', 'Sadar', '5', 82, '1', 3, 'Nyeri Sedang', 'pegal sedikit', 'engga ada sih', 'cepet obatin', 'di urut kusuk', '.kjnklb', 'Tidak Diterima'),
(61, '2018-05-25', 'Bayi', 'hamdani', 'Laki-laki', 'rumbai', '2018-02-05', '0 Tahun 3 Bulan 20 Hari ', 'jl. bujangan', 'UMUM', NULL, NULL, 11, 2, 'gak papa', 'ihkhbkj', 'Sadar', '3', 5, '8', 6, 'Nyeri Berat', 'jhkj', 'hbkjhbkh', 'khbkj', 'bkjh', 'masuk lah', 'Diterima'),
(62, '2018-05-26', 'Ibu', 'muliyani', 'Perempuan', 'pekanbaru', '2018-05-01', '0 Tahun 0 Bulan 25 Hari ', 'jl. karya', 'BPJS', 123, 43212, 10, 2, 'gak tau', 'iya ya', 'Tidak Sadar', '5', 7, '8', 6, 'Nyeri Sedang', 'langsung aja', 'sehat', 'tes sssd', 'kompres', 'sfsdf', 'Diterima'),
(68, '2018-05-26', 'Ibu', 'muliyani', 'Perempuan', 'pekanbaru', '2018-05-01', '0 Tahun 0 Bulan 25 Hari ', 'jl. karya', 'BPJS', 123, 43212, 10, 2, 'gak tau', 'iya ya', 'Tidak Sadar', '5', 7, '8', 6, 'Nyeri Sedang', 'langsung aja', 'sehat', 'tes sssd', 'kompres', 'iyaaa ok', 'Diterima'),
(69, '2018-05-26', 'Bayi', 'riki', 'Laki-laki', 'tampan', '2018-05-09', '0 Tahun 0 Bulan 17 Hari ', 'jl asdf', 'UMUM', NULL, NULL, 11, 2, 'kjkkj', 'kjjkb', 'Tidak Sadar', '8', 6, '7', 4, 'Nyeri Berat', 'h', 'hk', 'kjnkj', 'kjnkj', 'kjnkj', 'Diterima'),
(70, '2018-05-26', 'Ibu', 'jhbjkhb', 'Perempuan', 'jjkhbkjhb', '2018-05-17', '0 Tahun 0 Bulan 9 Hari ', 'jkhhbkjh', 'BPJS', 8786, 8688, 11, 2, 'jhjkh', 'jhbkhb', 'Tidak Sadar', '6', 8, '7', 5, 'Nyeri Sedang', 'jnjhb', 'lkjnlkj', 'kljnlknj', 'kjnlknj', 'asda', 'Diterima'),
(75, '2018-05-26', 'Ibu', 'koe iki', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'as', 'Diterima'),
(76, '2018-05-26', 'Ibu', 'paijan', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'gogogo', 'Tidak Diterima'),
(77, '2018-05-26', 'Ibu', 'yoko koko', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'hehe', 'Diterima'),
(78, '2018-05-26', 'Ibu', 'ok', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'hehe', 'Diterima'),
(79, '2018-05-26', 'Ibu', 'rizki ema', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'iya deh', 'Diterima'),
(80, '2018-05-26', 'Ibu', 'faejar', 'Perempuan', 'nljn', '2018-05-08', '0 Tahun 0 Bulan 18 Hari ', 'kjbnj', 'UMUM', NULL, NULL, 10, 2, 'kljn', 'jbljlj', 'Tidak Sadar', '8', 5, '6', 4, '', 'kj', 'khbkjhbk', 'hbkjhb', 'ljnklkj', 'heheh', 'Tidak Diterima'),
(81, '2018-06-02', 'Bayi', 'mbak pare', 'Perempuan', 'adfsaf', '2018-05-29', '0 Tahun 0 Bulan 4 Hari ', 'adsfaf', 'UMUM', NULL, NULL, 11, 2, 'jhbjkh', 'bjkhbjkbh', 'Sadar', '2', 5, '7', 8, 'Nyeri Berat', 'kjhbk', 'bkjhb', 'jhkjbh', 'khbkjhb', '', 'Menunggu'),
(83, '2018-06-02', 'Ibu', 'diani maharatu', 'Perempuan', 'adfsaf', '2018-05-29', '0 Tahun 0 Bulan 4 Hari ', 'adsfaf', 'UMUM', NULL, NULL, 11, 2, 'jhbjkh', 'bjkhbjkbh', 'Sadar', '2', 5, '7', 8, 'Nyeri Berat', 'kjhbk', 'bkjhb', 'jhkjbh', 'khbkjhb', '', 'Menunggu'),
(84, '2018-06-02', 'Ibu', 'bunda ami', 'Perempuan', 'adsfdsad', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'asdasd', 'UMUM', NULL, NULL, 11, 2, 'asdasd', 'jnkj', 'Tidak Sadar', '7', 5, '82.123', 7, 'Nyeri Berat', 'khvkhj', 'vkjhbkjh', 'jkhbkjhb', 'jhbkjhb', 'gavsadg', 'Tidak Diterima'),
(85, '2018-06-02', 'Bayi', 'jojo', 'Laki-laki', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(86, '2018-06-02', 'Ibu', 'pakde', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(87, '2018-06-02', 'Bayi', 'soso', '', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(88, '2018-06-02', 'Ibu', 'lala', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(89, '2018-06-02', 'Ibu', 'aasfsef', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(90, '2018-06-02', 'Ibu', 'pebryyyyyyyyyy', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(91, '2018-06-02', 'Ibu', 'bayi', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', '', 'Menunggu'),
(96, '2018-06-02', 'Ibu', 'harimau', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', 'ok', 'Tidak Diterima'),
(97, '2018-06-02', 'Ibu', 'kelinci', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', 'not available\r\n', 'Tidak Diterima'),
(98, '2018-06-02', 'Ibu', 'burung', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', 'available', 'Diterima'),
(99, '2018-06-02', 'Ibu', 'rikko efendi', 'Perempuan', ',jhbj', '2018-05-28', '0 Tahun 0 Bulan 5 Hari ', 'jbjkhb', 'UMUM', NULL, NULL, 10, 2, ',h', 'khvkjvkh', 'Tidak Sadar', '4', 7, '4', 8, 'Nyeri Berat', ',jhb', 'kjhbkjhbkjh', 'jkhbkh', 'jhbkjh', 'heheheh', 'Diterima'),
(100, '2018-06-04', 'Ibu', 'Jajat', 'Perempuan', 'jakarta', '2018-06-04', '0 Tahun 0 Bulan 0 Hari ', 'Jl. Taman Sari', 'UMUM', NULL, NULL, 10, 2, 'demam', 'asa', 'Tidak Sadar', '67', 4, '34', 8, 'Nyeri Sedang', 'gagal', 'bahaya', 'demam', 'restutasi', 'oke', 'Diterima'),
(101, '2018-06-05', 'Ibu', 'kiki ananda', 'Perempuan', 'tampan', '2018-05-27', '0 Tahun 0 Bulan 9 Hari ', 'jl. kartini', 'UMUM', NULL, NULL, 10, 2, 'sakit', 'iya', 'Sadar', '8', 7, '6', 5, 'Nyeri Sedang', 'aads', 'kjkj', 'kkj', 'basdas', 'boleh', 'Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `rujukan-b`
--

CREATE TABLE `rujukan-b` (
  `id` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `ibu_nama` varchar(150) NOT NULL,
  `ibu_tplahir` varchar(150) NOT NULL,
  `ibu_tglahir` date NOT NULL,
  `ibu_umur` tinyint(2) NOT NULL,
  `ibu_alamat` text NOT NULL,
  `ibu_carabayar` varchar(150) NOT NULL,
  `bayi_jk` varchar(25) NOT NULL,
  `bayi_nama` varchar(150) NOT NULL,
  `bayi_tplahir` varchar(11) NOT NULL,
  `bayi_tglahir` date DEFAULT NULL,
  `bayi_umur` tinyint(2) DEFAULT NULL,
  `bayi_alamat` text NOT NULL,
  `bayi_carabayar` varchar(150) NOT NULL,
  `asal_rujukan` int(11) DEFAULT NULL,
  `tujuan_rujukan` int(11) DEFAULT NULL,
  `tindakan_ygdiberikan` text NOT NULL,
  `alasan_rujukan` text NOT NULL,
  `diagnosa` text NOT NULL,
  `kesadaran` varchar(12) NOT NULL,
  `tekanan_darah` varchar(10) NOT NULL,
  `nadi` varchar(25) NOT NULL,
  `suhu` double DEFAULT NULL,
  `pernapasan` varchar(25) NOT NULL,
  `berat_badan` int(3) DEFAULT NULL,
  `tinggi_badan` int(4) DEFAULT NULL,
  `lila` varchar(50) NOT NULL,
  `nyeri` varchar(25) NOT NULL,
  `keterangan_lain` text NOT NULL,
  `info_balik` text NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rujukan-b`
--

INSERT INTO `rujukan-b` (`id`, `tgl_masuk`, `ibu_nama`, `ibu_tplahir`, `ibu_tglahir`, `ibu_umur`, `ibu_alamat`, `ibu_carabayar`, `bayi_jk`, `bayi_nama`, `bayi_tplahir`, `bayi_tglahir`, `bayi_umur`, `bayi_alamat`, `bayi_carabayar`, `asal_rujukan`, `tujuan_rujukan`, `tindakan_ygdiberikan`, `alasan_rujukan`, `diagnosa`, `kesadaran`, `tekanan_darah`, `nadi`, `suhu`, `pernapasan`, `berat_badan`, `tinggi_badan`, `lila`, `nyeri`, `keterangan_lain`, `info_balik`, `status`) VALUES
(7, '2018-05-05', 'satu', 'satu', '1970-01-01', 7, 'jb j jk', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'jhbjhkb', 'khbkjhb', 'kjhbkjbh', '', '', '', NULL, '', NULL, NULL, '', '', '', '', 'Diterima'),
(8, '2018-08-05', 'dua', 'dua', '1970-01-01', 7, 'kjkjhbkjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'bkjhbkjhbjh', 'kjhbkjhb', 'khbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', '', 'Diterima'),
(9, '2018-05-05', 'tiga', 'jkhbjbh', '1970-01-01', 87, 'jhbjkhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kjbkjhbkjh', 'hvhkvkhvkgh', 'hgvjhgvjhgvjh', '', '', '', NULL, '', NULL, NULL, '', '', '', '', 'Tidak Diterima'),
(10, '2018-05-06', 'Riani Rahayu', 'pekanbaru', '1970-01-01', 27, 'jl. beruang', 'BPJS', 'Laki-laki', 'Rio Ramadhan', 'pekanbaru', '1970-01-01', 1, 'jl. beruang', 'BPJS', 3, 2, 'melahirkan normal', 'tak kuat bang', 'enggak papa keknya', 'Sadar', '123', '456', 789, '101', 60, 158, 'iya', 'Nyeri Ringan', 'baik aja kok', '', 'Diterima'),
(54, '2018-05-06', 'asdasd', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asd', 'Tidak Diterima'),
(55, '2018-05-06', 'asdasd', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'bagus', 'Diterima'),
(56, '2018-05-06', 'sa', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asd', 'Diterima'),
(57, '2018-05-06', 'bule', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asd', 'Tidak Diterima'),
(58, '2018-05-06', 'hmmmmmmmm', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'bole laaaaaaaaaaaaa', 'Diterima'),
(59, '2018-05-06', 'bilo', 'pku', '1970-01-01', 65, 'jhjbjhb', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kaksdfjk', 'bkjhbkjhb', 'kjhbkjhbkjh', '', '', '', NULL, '', NULL, NULL, '', '', '', 'hehehe', 'Diterima'),
(60, '2018-05-07', 'dila dahlia', 'hbhjbkuyb', '1970-01-01', 87, 'jkhbkjhvkjh', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'jbkj', 'hbkjhvkhv', 'khvkhv', '', '', '', NULL, '', NULL, NULL, '', '', '', 'kamar masuk', 'Diterima'),
(61, '2018-05-07', 'dila dahlia', 'hbhjbkuyb', '1970-01-01', 87, 'jkhbkjhvkjh', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'jbkj', 'hbkjhvkhv', 'khvkhv', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asd', 'Diterima'),
(62, '2018-05-07', 'dila dahlia', 'hbhjbkuyb', '1970-01-01', 87, 'jkhbkjhvkjh', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'jbkj', 'hbkjhvkhv', 'khvkhv', '', '', '', NULL, '', NULL, NULL, '', '', '', 'go laah', 'Tidak Diterima'),
(63, '2018-05-07', 'dila dahlia', 'hbhjbkuyb', '1970-01-01', 87, 'jkhbkjhvkjh', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'jbkj', 'hbkjhvkhv', 'khvkhv', '', '', '', NULL, '', NULL, NULL, '', '', '', 'iya maak', 'Diterima'),
(64, '2018-05-07', 'karla ', 'sdsdf', '1970-01-01', 12, 'nkb lj', 'JKD', '', '', '', NULL, NULL, '', '', 8, 2, 'bljbjk', 'hbkjhbkjh', 'khbkjhb', '', '', '', NULL, '', NULL, NULL, '', '', '', 'terima', 'Diterima'),
(65, '2018-05-08', 'rahayu siska', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'langsung kesini aja', 'Diterima'),
(66, '2018-05-08', 'rahayu siska', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'bah', 'Diterima'),
(67, '2018-05-08', 'bayu', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asdad', 'Diterima'),
(68, '2018-05-08', 'ayu bah', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asdasd', 'Tidak Diterima'),
(69, '2018-05-08', 'hmmm', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asdfdsf', 'Tidak Diterima'),
(70, '2018-05-08', 'bude', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'asdasd', 'Diterima'),
(71, '2018-05-08', 'kakek', 'pku', '1970-01-01', 98, 'jl. keramat', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'lkjbkjhb', 'jhbkhvkh', 'vkhgvg', '', '', '', NULL, '', NULL, NULL, '', '', '', 'hemm', 'Tidak Diterima'),
(72, '2018-05-09', 'larni', 'jnjn', '1970-01-01', 87, 'kjnlk', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kjnkjb', 'kjhbkjhb', 'jhbjkhbjhb', '', '', '', NULL, '', NULL, NULL, '', '', '', 'm, ', 'Diterima'),
(73, '2018-05-09', 'larni', 'jnjn', '1970-01-01', 87, 'kjnlk', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kjnkjb', 'kjhbkjhb', 'jhbjkhbjhb', '', '', '', NULL, '', NULL, NULL, '', '', '', 'm m', 'Tidak Diterima'),
(74, '2018-05-09', 'bujang', 'jnjn', '1970-01-01', 87, 'kjnlk', 'UMUM', '', '', '', NULL, NULL, '', '', 3, 2, 'kjnkjb', 'kjhbkjhb', 'jhbjkhbjhb', '', '', '', NULL, '', NULL, NULL, '', '', '', 'dfds', 'Tidak Diterima');

-- --------------------------------------------------------

--
-- Table structure for table `rujukan-b-baru`
--

CREATE TABLE `rujukan-b-baru` (
  `id` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `jenis` varchar(4) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jk` varchar(25) NOT NULL,
  `tplahir` varchar(100) NOT NULL,
  `tglahir` date NOT NULL,
  `umur` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `cara_bayar` varchar(10) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `no_bpjs_jkd` int(16) NOT NULL,
  `asal_rujukan` int(11) NOT NULL,
  `tujuan_rujukan` int(11) NOT NULL,
  `alasan_rujukan` text NOT NULL,
  `anamnesa` text NOT NULL,
  `kesadaran` varchar(12) NOT NULL,
  `tekanan_darah` varchar(10) NOT NULL,
  `nadi` int(11) NOT NULL,
  `suhu` varchar(10) NOT NULL,
  `pernapasan` int(11) NOT NULL,
  `nyeri` varchar(25) NOT NULL,
  `pemeriksaan_fisik` text NOT NULL,
  `pemeriksaan_penunjang` text NOT NULL,
  `diagnosa` text NOT NULL,
  `tindakan_yg_sdh_diberikan` text NOT NULL,
  `info_balik` text NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rujukan`
--
ALTER TABLE `rujukan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rujukan-b`
--
ALTER TABLE `rujukan-b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rujukan-b-baru`
--
ALTER TABLE `rujukan-b-baru`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `rujukan`
--
ALTER TABLE `rujukan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `rujukan-b`
--
ALTER TABLE `rujukan-b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `rujukan-b-baru`
--
ALTER TABLE `rujukan-b-baru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
